package com.groundgurus.day3.exercises;

public class VideoGameApp {

    public static void main(String[] args) {
        var blackFalcon = VideoGameCharacter
                .builder()
                .name("Black Falcon")
                .hp(1000)
                .attack(500)
                .defense(300)
                .affinity(Affinity.GOOD)
                .build();

        var wackyBill = VideoGameCharacter
                .builder()
                .name("Wacky Bill")
                .hp(800)
                .attack(300)
                .defense(400)
                .affinity(Affinity.EVIL)
                .build();

        var fightingLocation = new FightingLocation();
        fightingLocation.fight(blackFalcon, wackyBill);

        System.out.println(blackFalcon);
        System.out.println(wackyBill);
    }
}
