package com.groundgurus.day3.exercises;

public enum Affinity {
    GOOD, NEUTRAL, EVIL
}
