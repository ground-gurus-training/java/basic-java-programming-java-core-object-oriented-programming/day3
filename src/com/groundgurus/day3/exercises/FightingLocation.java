package com.groundgurus.day3.exercises;

public class FightingLocation {

    public void fight(VideoGameCharacter character1, VideoGameCharacter character2) {
        var character1HP = character1.getHp();
        var character2HP = character2.getHp();
        
        // character 1 attacks character 2
        character2.setHp(character2HP - character1.getAttack());

        // chracter 2 attacks character 1
        character1.setHp(character1HP - character2.getAttack());
    }
}
