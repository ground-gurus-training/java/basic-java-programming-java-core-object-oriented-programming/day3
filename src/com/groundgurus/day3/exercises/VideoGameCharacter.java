package com.groundgurus.day3.exercises;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class VideoGameCharacter {
    private String name;
    private int hp;
    private int attack;
    private int defense;
    private Affinity affinity;
}
